#ESDK Client API

An API for abas App development using ESDK.

For details refer to the [general ESDK documentation](https://documentation.abas.cloud/en/esdk/#_use_the_esdk_client_api).

## License checker

To be able to validate an app's license, team Fusion's licensing client API is used.

* Sources: Project [license-api on Bitbucket](https://bitbucket.org/abascloud/license-api)
* Releases: Package `com.abas.licensing.client.api` in repository [abas.releases](https://registry.abas.sh/artifactory/webapp/#/artifacts/browse/tree/General/abas.releases/com/abas/licensing/client/api)

## Development
To build, test and check your changes run:

```
./gradlew :build
```

To build and test the ESDK App for integration test verification run:

```
./gradlew testClientApi:checkPreconditions testClientApi:fullInstall testClientApi:verify
```

### Artifacts
To build the library JAR run:
```
./gradlew :jar
```

The JAR file will be located as esdk-client-api-{version}.jar in build/libs.

### Spotless
If `spotlessCheck` fails run `./gradlew spotlessApply`

### Documentation
Run `./gradlew javadoc` to generate the Javadoc.

It can then be accessed from `build/docs/javadoc/index.html`
